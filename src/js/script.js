let title = document.querySelector("#title");
const timer_bt = document.querySelector("#timer");
let screen = document.querySelector("#screen");
let content = document.querySelector("#content");
const play_bt = document.querySelector("#play");
const pause_bt = document.querySelector("#pause");
const stop_bt = document.querySelector("#stop");
const clock_bt = document.querySelector("#clock");
const stopwatch_bt = document.querySelector("#stopwatch");
const add_bts = document.querySelector("#add");
const tools_bts = document.querySelector("#tools");
const remove_bts = document.querySelector("#remove");

let interID = setInterval(() => {
  screen.innerHTML = new Date(Date.now()).toLocaleTimeString();
}, 0);

clock_bt.addEventListener('click', () => {
  title.innerText = "Clock"
  if (interID) {
    clearInterval(interID);
    interID = null;
  }
  add_bts.classList.add("hide");
  tools_bts.classList.add("hide");
  remove_bts.classList.add("hide");
  if (!interID) {
    interID = setInterval(() => {
      screen.innerHTML = new Date(Date.now()).toLocaleTimeString();
    }, 0);
  }
})

stopwatch_bt.addEventListener('click', () => {
  title.innerText = "Stopwatch";
  if (interID) {
    clearInterval(interID);
    interID = null;
  }
  screen.innerHTML = "00:00:00"
  add_bts.classList.add("hide");
  tools_bts.classList.remove("hide");
  remove_bts.classList.add("hide");
});

timer_bt.addEventListener('click', () => {
  title.innerText = "Timer"
  if (interID) {
    clearInterval(interID);
    interID = null;
  }
  screen.innerHTML = "00:00:00"
  add_bts.classList.remove("hide");
  tools_bts.classList.remove("hide");
  remove_bts.classList.remove("hide");
})

play_bt.addEventListener('click', () => {
  add_bts.classList.add("hide");
  remove_bts.classList.add("hide");
  if (title.innerText == "Stopwatch") {
    let time = screen.innerText.split(":");
    let milisec = Number(time[2]),
      sec = Number(time[1]),
      min = Number(time[0]);
    if (!interID) {
      interID = setInterval(() => {
        milisec++;
        if (milisec == 100) {
          sec++;
          milisec = 0
        }
        if (sec == 60) {
          min++;
          sec = 0
        }
        screen.innerText = checkTime(min) + ":" + checkTime(sec) + ":" + checkTime(milisec);
      }, 10)
    }
  } else {
    let time = screen.innerText.split(":");
    let hour = Number(time[0]);
    let min = Number(time[1]);
    let sec = Number(time[2]);
    if (!interID) {
      interID = setInterval(() => {
        if (sec == 0) {
          if (min != 0) {
            sec = 59;
            min--;
          } else {
            if (hour != 0) {
              sec = 59;
              min = 59;
              hour--;
            }
          }
        } else {
          sec--;
        }


        screen.innerText = checkTime(hour) + ":" + checkTime(min) + ":" + checkTime(sec);
        if (hour == 0 && min == 0 && sec == 0) {
          clearInterval(interID);
          interID = null;
          interID = setInterval(() => {
            screen.style.backgroundColor == "red" ? screen.style.backgroundColor = "white" : screen.style.backgroundColor = "red";
          }, 300)
        }
      }, 1000)
    }
  }
})

pause_bt.addEventListener('click', () => {
  clearInterval(interID);
  interID = null;
  screen.style.backgroundColor = "white";
  if (title.innerText == "Timer") {
    add_bts.classList.remove("hide");
    remove_bts.classList.remove("hide");
  }

})

stop_bt.addEventListener('click', () => {
  clearInterval(interID);
  interID = null;
  screen.style.backgroundColor = "white";
  screen.innerText = "00:00:00";
  if (title.innerText == "Timer") {
    add_bts.classList.remove("hide");
    remove_bts.classList.remove("hide");
  }
})

function checkTime(t) {
  let i;
  if (t < 10) {
    i = "0" + t;
  } else {
    i = t;
  }
  return i;
}

function modifyTimer(unit, operator) {
  let timer = screen.innerText.split(":");
  let h = Number(timer[0]), m = Number(timer[1]), s = Number(timer[2]);
  if (operator == '+') {
    if (unit == 'h') {
      h == 59 ? h = 0 : h++;
    } else if (unit == 'm') {
      m == 59 ? (m = 0, h++) : m++;
    } else {
      s == 59 ? (s = 0, m++) : s++;
    }
  } else {
    if (unit == 'h') {
      h == 0 ? h = 59 : h--;
    } else if (unit == 'm') {
      m == 0 ? (m = 59, h--) : m--;
    } else {
      s == 0 ? (s = 59, m--) : s--;
    }
  }
  screen.innerText = checkTime(h) + ":" + checkTime(m) + ":" + checkTime(s);
}